---
title: LivePose
subtitle: Democratizing Pose Detection for Multimedia Arts and Telepresence Applications on Open Edge Devices
author: Christian Frisson, Gabriel N. Downs, Marie-Ève Dumas, Farzaneh Askari, Emmanuel Durand
institute: Société des Arts Technologiques [SAT]
separator: <!--s-->
verticalSeparator: <!--v-->
theme: white 
revealOptions:
  transition: 'none' 
  loop: true
  slideNumber: true
  autoPlayMedia: true
--- 

<!-- .slide: id="header" -->
  <h2 data-i18n="title">LivePose: </h2>
  <h3 data-i18n="subtitle">Democratizing Pose Detection for Multimedia Arts and Telepresence Applications on Open Edge Devices</h3>
  <h4 data-i18n="conference">28th ACM Symposium on Virtual Reality Software and Technology (VRST 2022)</h4>
  <img src="images/logos/VRST2022_logo.png" style="height:70px;" />  

[https://gitlab.com/sat-mtl/tools/livepose](https://gitlab.com/sat-mtl/tools/livepose)

  <small>Christian Frisson, Gabriel N. Downs, Marie-Ève Dumas, Farzaneh Askari, Emmanuel Durand</small>

[[SAT](https://sat.qc.ca/)] [metalab](https://sat-mtl.gitlab.io/metalab/)

<!--s-->
  <!-- .slide: id="sat" -->
  <h2 data-i18n="title"><a href="https://sat.qc.ca">Society for Arts and Technology</a></h2>
  <img src="images/sat.svg" style="height:700px;margin-top:-50px" />  


<!--s-->
  <!-- .slide: id="livepose-experimentations" -->
  <h2 data-i18n="title">Interaction for Immersion</h2>
  <iframe src="https://player.vimeo.com/video/639532760"  data-audio-controls  width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

<!--v-->
  <!-- .slide: id="satosphere" -->
  <h2 data-i18n="title">In The Satosphere</h2>
  <video autoplay controls src="images/livepose/openpose_walk_0.mp4" data-audio-controls width="100%" height="auto" />

<!--s-->
<!-- .slide: id="livepose-detection" -->
  <h2 data-i18n="title">Action and Gesture Detection</h2>
  <p data-i18n="paragraph1">Control an interactive environment with gestures like raising arms, jumping, etc.</p>

![](images/livepose/arms_up.png)


<!--v-->
<!-- .slide: id="pose-estimation" -->
  <h2 data-i18n="title">Pose estimation</h2>
  <p data-i18n="paragraph1">Goal: To estimate the pose of a person in an image by locating special body points (keypoints)</p>

![](images/livepose/keypoints_and_people.png)


<!--s-->
<!-- .slide: id="livepose-architecture" -->
  <h2 data-i18n="title">Architecture</h2>

[Frame(s) Capture](#livepose-frame-capture)

&#8595;

[Pose Backend(s)](#livepose-pose-backends)

&#8595;

[Dimension Mapping(s)](#livepose-dimension-mapping)

&#8595;

[Filter(s)](#livepose-filter)

&#8595;

[Output(s)](#livepose-output)


<!--v-->
<!-- .slide: id="livepose-frame-capture" -->
  <h2 data-i18n="title"><a href="#livepose-architecture">Architecture</a></h2>
  <h3 data-i18n="title">Frame Capture</h3>

* [OpenCV](https://github.com/opencv/opencv): including, but not limited to: 
  * V4L2 compatible cameras, 
  * MP4 video files, 
  * and JPG image files
* [pyrealsense2](https://github.com/IntelRealSense/librealsense): 
  * Intel Realsense depth cameras,
  * ROSbag recorded files

<!--v-->
<!-- .slide: id="livepose-pose-backends" -->
  <h2 data-i18n="title"><a href="#livepose-architecture">Architecture</a></h2>
  <h3 data-i18n="title">Pose Estimation Backends</h3>
  <ul>
  <li data-i18n="MediaPipe"><a href='https://github.com/google/mediapipe'>MediaPipe</a> (Google): default on many CPUs/GPUs</li>
  <li data-i18n="MMPose"><a href='https://github.com/open-mmlab/mmpose'>MMPose</a> (OpenMMLab): extensive collection of algorithms from the newest research works.</li>  
  <li data-i18n="PoseNet"> <a href='https://github.com/tensorflow/tfjs-models/tree/master/posenet'>PoseNet</a> (TensorFlow): deprecated (by MediaPipe)</li>
  <li data-i18n="OpenPose"><a href='https://github.com/CMU-Perceptual-Computing-Lab/openpose'>OpenPose</a> (CMU): deprecated (limitative license)</li>
  <li data-i18n="TRTPose"><a href='https://github.com/NVIDIA-AI-IOT/trt_pose'>TRTPose</a> (NVIDIA): default on NVIDIA GPUs and Jetson boards, optimized for using TensorRT</li>
  </ul>

<!--v-->
<!-- .slide: id="livepose-dimension-mapping" -->
  <h2 data-i18n="title"><a href="#livepose-architecture">Architecture</a></h2>
  <h3 data-i18n="title">Dimension Mapping</h3>

* currently: from 2D poses (in the camera frame) to
floor position. 
* future: pose tracking, pose reidentification and 2D to 3D conversion

<!--v-->
<!-- .slide: id="livepose-filter" -->
  <h2 data-i18n="title"><a href="#livepose-architecture">Architecture</a></h2>
  <h3 data-i18n="title">Filter</h3>

* tasks like detecting arms up, the overall orientation of a body, or whether a person is
pointing at an object
* adapting data for specific uses including controlling users’ avatars in Mozilla Hubs

<!--v-->
<!-- .slide: id="livepose-output" -->
  <h2 data-i18n="title"><a href="#livepose-architecture">Architecture</a></h2>
  <h3 data-i18n="title">Output</h3>
  <ul>
  <li data-i18n="libmapper"><a href='https://github.com/libmapper/libmapper/'>libmapper</a>: for digital musical instruments</li>
  <li data-i18n="osc"><a href='https://www.opensoundcontrol.org/'>OSC</a> (Open Sound Control): for sound and lighting interactive environments</li>
  <li data-i18n="websocket"><a href='https://en.wikipedia.org/wiki/WebSocket'>WebSocket</a>: for web applications</li>
  </ul>
  <iframe src="https://player.vimeo.com/video/604196712?muted=1" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture;" allowfullscreen></iframe>

<!--s-->
<!-- .slide: id="livepose-distribution" -->

  <h2 data-i18n="title">Distribution</h2>
  <h3>LivePose and dependencies</h3>
  <ul>
  <li data-i18n="mpa">Packaged with CUDA acceleration: 
  <ul>
  <li data-i18n="mpa"><a href='https://gitlab.com/sat-mtl/distribution/mpa-jammy-amd64-nvidia'>mpa-jammy-amd64-nvidia</a></li>
  <li data-i18n="mpa"><a href='https://gitlab.com/sat-mtl/distribution/mpa-focal-arm64-jetson'>mpa-focal-arm64-jetson</a></li>
  </li>
  </ul>
  <li data-i18n="jetson-images">Pre-configured and pre-installed: 
  <ul>
  <li data-i18n="mpa"><a href='https://gitlab.com/sat-mtl/distribution/jetson-images'>jetson-image</a></li>
   </li>
  </ul>
  </ul>

<!--s-->
<!-- .slide: id="livepose-integration" -->
  <h2 data-i18n="title">Integration</h2>

* [Chataigne](#livepose-integration-chataigne)
* [libmapper](#livepose-integration-libmapper)
* [OSSIA](#livepose-integration-ossia)


<!--v-->
  <!-- .slide: id="livepose-integration-chataigne" -->
  <h2 data-i18n="title"><a href="#livepose-integration">Integration</a></h2>
  <h3 data-i18n="title">Chataigne</h3>

[LivePose](https://gitlab.com/sat-mtl/tools/livepose/) +
[Chataigne](https://github.com/benkuper/Chataigne) +
[SATIE](https://gitlab.com/sat-metalab/satie/)

  <iframe src="https://player.vimeo.com/video/639244396" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

<!--v-->
  <!-- .slide: id="livepose-integration-libmapper" -->
  <h2 data-i18n="title"><a href="#livepose-integration">Integration</a></h2>
  <h2 data-i18n="title">libmapper</h2>

[LivePose](https://gitlab.com/sat-mtl/tools/livepose/) +
[libmapper](https://libmapper.github.io/) +
[SATIE](https://gitlab.com/sat-metalab/satie/)

  <iframe width="560" height="315" src="https://www.youtube.com/embed/0exFzDgdrT4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<!--v-->
  <!-- .slide: id="livepose-integration-ossia" -->
  <h2 data-i18n="title"><a href="#livepose-integration">Integration</a></h2>
  <h2 data-i18n="title">OSSIA</h2>

[LivePose](https://gitlab.com/sat-mtl/tools/livepose/) +
[OSSIA](https://ossia.io/) +
[SATIE](https://gitlab.com/sat-metalab/satie/)

  <iframe src="https://player.vimeo.com/video/647496450" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

